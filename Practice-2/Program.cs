using Practice_2_Drozdov.Facade;
using Practice_2_Drozdov.FactoryMethod;
using Practice_2_Drozdov.Observer.Observer;

namespace Practice_2_Drozdov;

class Program
{
    static void Main(string[] args)
    {
        var detectiveBookFactory = new DetectiveFactory();
        var novelBookFactory = new DetectiveFactory();
        IBookFacade bookFacade = new BookFacade();
        var employee = new Employee(bookFacade);
        var detectiveBook = detectiveBookFactory.CreateBook("Записки о Шерлоке Холмсе: Этюд в багровых тонах");
        var novelBook = novelBookFactory.CreateBook("Гипотеза любви");
        
        employee.PlaceOrder(detectiveBook);
        // Отрицание можно поставить/убрать
        if (!employee.SearchBookByTitle("Гипотеза любви"))
        {
            employee.PlaceOrder(novelBook);
        }
    }
}
