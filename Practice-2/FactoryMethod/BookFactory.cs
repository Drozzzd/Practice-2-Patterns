namespace Practice_2_Drozdov.FactoryMethod;

public abstract class BookFactory
{
    public abstract Book CreateBook(string name);
}

public class DetectiveFactory : BookFactory
{
    public override Book CreateBook(string name)
    {
        return new DetectiveBook(name);
    }
}

public class ScienceFictionFactory : BookFactory
{
    public override Book CreateBook(string name)
    {
        return new ScienceFictionBook(name);
    }
}

public class NovelFactory : BookFactory
{
    public override Book CreateBook(string name)
    {
        return new NovelBook(name);
    }
}
